<?php

namespace App\Components;

use App\Contracts\SuperContract;

class SuperComponent implements SuperContract
{
    protected $name = 'super ';

    public function __construct($attribute)
    {
        $this->name .= $attribute;
    }

    public function getName()
    {
        return $this->name;
    }
}