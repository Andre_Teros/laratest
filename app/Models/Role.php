<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property array $permissions
 */
class Role extends Model
{
    public const ROLE_AUTHOR = 'author';

    public $timestamps = false;

    protected $table = 'roles';

    protected $fillable = [
        'name',
        'slug',
        'permissions'
    ];

    protected $casts = [
        'permissions' => 'array'
    ];

    public function users()
    {
        return $this->belongsToMany(
            User::class,                       // link to related model
            'roles_user',                        // intermediate table
            'roleId',                   // field of intermediate table linked to this Model
            'userId'                    // field of intermediate table linked to related Model
        );
    }

    public function hasAccess(array $permissions): bool
    {
        foreach ($permissions as $permission) {
            if ($this->hasPermission($permission)) {
                return true;
            }
        }

        return false;
    }

    public function hasPermission($permission): bool
    {
        return $this->permissions[$permission] ?? false;
    }
}
