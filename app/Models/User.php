<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App\Models
 *
 * @property \App\Models\Role[] $roles
 */
class User extends Authenticatable
{
    use Notifiable;

    public const CREATED_AT = 'createdAt';
    public const UPDATED_AT = 'updatedAt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            Role::class,                       // link to related model
            'roles_user',                        // intermediate table
            'userId',                   // field of intermediate table linked to this Model
            'roleId'                    // field of intermediate table linked to related Model
        );
    }

    public function hasAccess(array $permissions): bool
    {
        foreach ($this->roles as $role) {
            if ($role->hasAccess($permissions)) {
                return true;
            }
        }

        return false;
    }

    public function hasRole($roleSlug): bool
    {
        return $this->roles()->where('slug', $roleSlug)->count() === 1;
    }
}
