<?php

namespace App\Facades;

use App\Components\SuperComponent;
use Illuminate\Support\Facades\Facade;

/**
 * Class SuperFacade
 * @package App\Facades
 *
 * @method static string getName()
 *
 * @see \App\Components\SuperComponent
 */
class SuperFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return SuperComponent::class;
    }
}