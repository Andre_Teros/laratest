<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use App\Models\Role;

class AssignRole extends Command
{
    private const ARGUMENT_ROLE = 'role';
    private const ARGUMENT_USER_ID = 'usedId';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assign:role {' . self::ARGUMENT_ROLE . '} {' . self::ARGUMENT_USER_ID . '}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign role to user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $slug = $this->argument(self::ARGUMENT_ROLE);
            $userId = $this->argument(self::ARGUMENT_USER_ID);

            $role = Role::where('slug', $slug)->first();
            if ($role === null) {
                $this->error("Invalid role $role");
            }

            /** @var \App\Models\User $user */
            $user = User::where('id', $userId)->first();
            if ($user === null) {
                $this->error("Invalid user id $userId");
            }

            $user->roles()->attach($role);

            $this->info("User $userId now has role $slug");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
