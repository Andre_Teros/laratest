<?php

namespace App\Http\Controllers;

use App\Components\SuperComponent;
use App\Contracts\SuperContract;
use App\Facades\SuperFacade;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    public function index(SuperContract $s)
    {
        $a = 6;

        switch ($a) {
            case 0:

                break;

            case 1:
                $u = User::find('1');

                /** @var \App\Models\User $u */
                $roles = $u->roles();
                d($u->roles);

//                try {
//                    $roles->attach(10);
//                } catch (\Illuminate\Database\QueryException $q) {
//                    d($q);
//                }

                break;

            case 2:
                $role = \App\Models\Role::where('slug', 'author')->first();
                d($role);

                $role = \App\Models\Role::where('slug', 'author1')->first();
                d($role);

                break;

            case 3:
                d(Auth::user());

                break;              // current user

            case 4:
                $post = Post::findOrFail(10);

                break;              // test findOrFail

            case 5:

                $post = new Post();
                var_dump($post->save());

                break;              // try to save Post

            case 6:

                var_dump($s->getName());

                break;              // inject object use Provider

            case 7:

                return view('test');

                break;              // get access to object method use Facade
        }
    }


}
