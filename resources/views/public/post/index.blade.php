@extends('layouts.app')

@section('content')

    @include('partial.success')

    <div class="conteiner">

        <a href="{{ route('post.create') }}" class="btn btn-outline-success">Create</a>

        <table class="table table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Status</th>
                <th>Operations</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                @if(count($posts))
                    @foreach($posts as $post)
                        <tr>
                            <td>{{ $post->id }}</td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->published }}</td>
                            <td>
                                <a href="{{ route('post.show', ['id' => $post->id]) }}" class="btn btn-outline-success">Show</a>
                                <a href="{{ route('post.edit', ['id' => $post->id]) }}" class="btn btn-outline-success">Edit</a>
                            </td>
                            <td>
                                <form action="{{ route('post.destroy', ['id' => $post->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
@endsection