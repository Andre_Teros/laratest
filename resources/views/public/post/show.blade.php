@extends('layouts.app')

@section('content')

    <div class="conteiner">
        <a href="{{ route('post.index') }}" class="btn btn-outline-success">List</a>

        <h2>{{ $post->title }}</h2>
        <p>{{ $post->body }}</p>

    </div>
@endsection