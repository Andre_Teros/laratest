@extends('layouts.app')

@section('content')

    @include('partial.error')

    <div class="conteiner">
        <div class="row">
            <div class="col-md-offset-4 col-md-4">
                <form action="{{ route('post.store') }}" method="post">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="title">title</label>
                        <input type="text" class="form-control" id="title" name="title">
                    </div>

                    <div class="form-group">
                        <label for="body">body</label>
                        <textarea type="text" class="form-control" id="body" name="body"></textarea>
                    </div>

                    <input type="submit" value="Submit" class="btn btn-success">

                </form>
            </div>
        </div>
    </div>
@endsection