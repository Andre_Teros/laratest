@if ($errors->any())
    <div class="alert alert-danger">
        @foreach ($errors->getBags() as $error)
            <p>{{ $error }}</p>
        @endforeach

        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif